module.exports = {
    shouldDraw: function (x, y) {
        var realComponentOfResult = x;
        var imaginaryComponentOfResult = y;

        for(var i = 0; i < 10; i++) {
            // Calculate the real and imaginary components of the result
            // separately
            var tempRealComponent = realComponentOfResult * realComponentOfResult
                - imaginaryComponentOfResult * imaginaryComponentOfResult
                + x;

            var tempImaginaryComponent = 2 * realComponentOfResult * imaginaryComponentOfResult
                + y;

            realComponentOfResult = tempRealComponent;
            imaginaryComponentOfResult = tempImaginaryComponent;
        }

        if (realComponentOfResult * imaginaryComponentOfResult < 5)
            return true; // In the Mandelbrot set

        return false; // Not in the set
    }
};