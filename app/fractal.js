module.exports = {
    writeFractal: function writeFractal(renderer, algorithm) {
        var width = renderer.getWidth();
        var height = renderer.getHeight()

        var magnify = 600;
        var panX = 0;
        var panY = 0;

        for (var x = 0; x < width; x++) {
            for (var y = 0; y < height; y++) {
                var belongsToSet = algorithm.shouldDraw(x / magnify - panX, y / magnify - panY);
                if (belongsToSet) {
                    renderer.draw(x, y, 1, 1); // Draw a black pixel
                }
            }
        }
    }
};
