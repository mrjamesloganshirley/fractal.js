var fractal = require('../app/fractal.js');
var mandelbrot = require('../app/src/algorithms/mandelbrot.js');
describe('app', function() {
    describe('#writeFractal(renderer, algorithm)', function() {
        it('okay', function() {
            fractal.writeFractal({
                    "getWidth": function () {
                        return 20
                    }, "getHeight": function () {
                        return 10
                    }, "draw": function (x, y) {
                        console.log("Pixel drawn at: [" + x + "][" + y + "]")
                    }
                }, mandelbrot)
        });
    });
});